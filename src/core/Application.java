package core;
import javax.swing.*;

public class Application extends JFrame implements Runnable
{
    private ChessBoard chessBoard;
    private Thread renderingThread;


    public Application(int width, int height, String title)
    {
        renderingThread = new Thread(this);

        chessBoard = new ChessBoard();
        add(chessBoard);

        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setSize(width, height);
        setTitle(title);
        setVisible(true);
        setResizable(false);

        renderingThread.start();
    }

    @Override
    public void run()
    {
        while (true)
        {
            chessBoard.repaint();
        }
    }
}
