package core;

public class Move
{
    public int fromX;
    public int fromY;
    public int toX;
    public int toY;

    public Move(int fromx, int fromy, int tox, int toy)
    {
        fromX = fromx;
        fromY = fromy;
        toX = tox;
        toY = toy;
    }
}
