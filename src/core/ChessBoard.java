package core;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.awt.image.BufferedImage;
import java.awt.image.ImageObserver;
import java.io.File;
import java.util.ArrayList;

public class ChessBoard extends JPanel implements ComponentListener, MouseListener
{
    private int[][][] boardMap;

    private static final int ILLEGAL_MOVE = -1;

    private static final int NONE = 0;
    private static final int PAWN = 1;
    private static final int QUEEN = 9;
    private static final int ROOK = 5;
    private static final int KNIGHT = 2;
    private static final int BISHOP = 3;
    private static final int KING = 1000;
    private static final int WHITE = 1;
    private static final int BLACK = 2;

    private BufferedImage originalBlackKnight;
    private BufferedImage originalBlackQueen;
    private BufferedImage originalBlackKing;
    private BufferedImage originalBlackPawn;
    private BufferedImage originalBlackBishop;
    private BufferedImage originalBlackRook;
    private BufferedImage originalWhiteKnight;
    private BufferedImage originalWhiteQueen;
    private BufferedImage originalWhiteKing;
    private BufferedImage originalWhiteBishop;
    private BufferedImage originalWhiteRook;
    private BufferedImage originalWhitePawn;


    private Image adjustedBlackKnight;
    private Image adjustedBlackQueen;
    private Image adjustedBlackKing;
    private Image adjustedBlackPawn;
    private Image adjustedBlackBishop;
    private Image adjustedBlackRook;
    private Image adjustedWhiteKnight;
    private Image adjustedWhiteQueen;
    private Image adjustedWhiteKing;
    private Image adjustedWhiteBishop;
    private Image adjustedWhiteRook;
    private Image adjustedWhitePawn;

    private ImageObserver imageObserver;

    private int cellWidth;
    private int cellHeight;

    private boolean IsMousePressed;
    private boolean WasMousePressed;
    private int[] figureSelected;
    private int[][] highlightMap;



    public ChessBoard()
    {
        boardMap = new int[8][8][2];
        highlightMap = new int[8][8];
        figureSelected = new int[2];

        addComponentListener(this);
        addMouseListener(this);

        imageObserver = new ImageObserver()
        {
            @Override
            public boolean imageUpdate(Image img, int infoflags, int x, int y, int width, int height)
            {
                return false;
            }
        };

        convertFromFen("rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR");

        System.out.println(boardMap[1][0][0] == KNIGHT);

        try
        {
            originalWhitePawn = ImageIO.read(new File("res/sprites/white_pawn.gif"));
            originalWhiteRook = ImageIO.read(new File("res/sprites/white_rook.gif"));
            originalWhiteKnight = ImageIO.read(new File("res/sprites/white_knight.gif"));
            originalWhiteKing = ImageIO.read(new File("res/sprites/white_king.png"));
            originalWhiteQueen = ImageIO.read(new File("res/sprites/white_queen.gif"));
            originalWhiteBishop = ImageIO.read(new File("res/sprites/white_bishop.gif"));



            originalBlackPawn = ImageIO.read(new File("res/sprites/black_pawn.gif"));
            originalBlackRook = ImageIO.read(new File("res/sprites/black_rook.gif"));
            originalBlackKnight = ImageIO.read(new File("res/sprites/black_knight.gif"));
            originalBlackKing = ImageIO.read(new File("res/sprites/black_king.gif"));
            originalBlackQueen = ImageIO.read(new File("res/sprites/black_queen.gif"));
            originalBlackBishop = ImageIO.read(new File("res/sprites/black_bishop.gif"));

        }
        catch (Exception e)
        {
            System.out.println(e);
        }
    }

    @Override
    protected void paintComponent(Graphics g)
    {
        super.paintComponent(g);

        g.setColor(Color.WHITE);
        g.fillRect(0, 0, getWidth(), getHeight());

        for (int i = 0; i < boardMap.length; i++)
        {
            for (int j = 0; j < boardMap[0].length; j++)
            {
                g.setColor(Color.WHITE);

                if ((i + j + 2) % 2 == 0)
                {
                    g.setColor(new Color(114, 153, 84));
                }

                if (highlightMap[i][j] == ILLEGAL_MOVE)
                {
                    g.setColor(new Color(231, 80, 96));
                }

                g.fillRect(i * cellWidth, j * cellHeight, cellWidth, cellHeight);

                drawFigure(g, boardMap[i][j][0], boardMap[i][j][1], i * cellWidth, j * cellHeight);

                if (IsMousePressed)
                {
                    drawFigure(g, boardMap[figureSelected[0]][figureSelected[1]][0], boardMap[figureSelected[0]][figureSelected[1]][1], getMousePosition().x - 37, getMousePosition().y - 30);
                }
            }
        }
    }

    public void instantiateFigure(int figure, int color, int x, int y)
    {
        boardMap[x][y][0] = figure;
        boardMap[x][y][1] = color;
    }

    private void drawFigure(Graphics g, int figure, int color, int x, int y)
    {

        if (figure == PAWN)
        {
            if (color == WHITE) g.drawImage(adjustedWhitePawn, x, y, imageObserver);
            if (color == BLACK) g.drawImage(adjustedBlackPawn, x, y, imageObserver);
        }

        if (figure == QUEEN)
        {
            if (color == WHITE) g.drawImage(adjustedWhiteQueen,  x, y, imageObserver);
            if (color == BLACK) g.drawImage(adjustedBlackQueen,  x, y, imageObserver);
        }

        if (figure == BISHOP)
        {
            if (color ==  WHITE) g.drawImage(adjustedWhiteBishop,  x, y, imageObserver);
            if (color ==  BLACK) g.drawImage(adjustedBlackBishop,  x, y, imageObserver);
        }

        if (figure == KING)
        {
            if (color == WHITE) g.drawImage(adjustedWhiteKing,  x, y,  imageObserver);
            if (color == BLACK) g.drawImage(adjustedBlackKing,  x, y,  imageObserver);
        }

        if (figure == ROOK)
        {
            if (color == WHITE) g.drawImage(adjustedWhiteRook,  x, y, imageObserver);
            if (color == BLACK) g.drawImage(adjustedBlackRook,  x, y,  imageObserver);
        }

        if (figure == KNIGHT)
        {
            if (color == WHITE) g.drawImage(adjustedWhiteKnight,  x, y, imageObserver);
            if (color == BLACK) g.drawImage(adjustedBlackKnight,  x, y,  imageObserver);
        }

    }

    @Override
    public void componentResized(ComponentEvent e)
    {
        cellWidth = e.getComponent().getWidth() / 8;
        cellHeight = e.getComponent().getWidth() / 8;

        adjustedWhitePawn = originalWhitePawn.getScaledInstance(cellWidth, cellHeight, Image.SCALE_DEFAULT);
        adjustedWhiteKing = originalWhiteKing.getScaledInstance(cellWidth, cellHeight, Image.SCALE_DEFAULT);
        adjustedWhiteQueen = originalWhiteQueen.getScaledInstance(cellWidth, cellHeight, Image.SCALE_DEFAULT);
        adjustedWhiteRook = originalWhiteRook.getScaledInstance(cellWidth, cellHeight, Image.SCALE_DEFAULT);
        adjustedWhiteBishop = originalWhiteBishop.getScaledInstance(cellWidth, cellHeight, Image.SCALE_DEFAULT);
        adjustedWhiteKnight = originalWhiteKnight.getScaledInstance(cellWidth, cellHeight, Image.SCALE_DEFAULT);

        adjustedBlackPawn = originalBlackPawn.getScaledInstance(cellWidth, cellHeight, Image.SCALE_DEFAULT);
        adjustedBlackKing = originalBlackKing.getScaledInstance(cellWidth, cellHeight, Image.SCALE_DEFAULT);
        adjustedBlackQueen = originalBlackQueen.getScaledInstance(cellWidth, cellHeight, Image.SCALE_DEFAULT);
        adjustedBlackRook = originalBlackRook.getScaledInstance(cellWidth, cellHeight, Image.SCALE_DEFAULT);
        adjustedBlackBishop = originalBlackBishop.getScaledInstance(cellWidth, cellHeight, Image.SCALE_DEFAULT);
        adjustedBlackKnight = originalBlackKnight.getScaledInstance(cellWidth, cellHeight, Image.SCALE_DEFAULT);
    }

    public void convertFromFen(String fen)
    {
        int yCoord = 0;
        int xCoord = 0;

        for (byte fenByte : fen.getBytes())
        {
            if (fenByte == '/' && yCoord < 8)
            {
                xCoord = 0;
                yCoord++;
                continue;
            }


            if (Character.isDigit(fenByte))
            {
                xCoord += Character.getNumericValue((fenByte));
            }
            else
            {
                xCoord += 1;
                System.out.println(xCoord);
            }


            instantiateFigure(NONE, NONE, xCoord - 1, yCoord);

            if (fenByte == 'p') instantiateFigure(PAWN, BLACK, xCoord - 1, yCoord);
            if (fenByte == 'P') instantiateFigure(PAWN, WHITE, xCoord - 1, yCoord);

            if (fenByte == 'b') instantiateFigure(BISHOP, BLACK, xCoord - 1, yCoord);
            if (fenByte == 'B') instantiateFigure(BISHOP, WHITE, xCoord - 1, yCoord);

            if (fenByte == 'r') instantiateFigure(ROOK, BLACK, xCoord - 1, yCoord);
            if (fenByte == 'R') instantiateFigure(ROOK, WHITE, xCoord - 1, yCoord);

            if (fenByte == 'n') instantiateFigure(KNIGHT, BLACK, xCoord - 1, yCoord);
            if (fenByte == 'N') instantiateFigure(KNIGHT, WHITE, xCoord - 1, yCoord);

            if (fenByte == 'k') instantiateFigure(KING, BLACK, xCoord - 1, yCoord);
            if (fenByte == 'K') instantiateFigure(KING, WHITE, xCoord - 1, yCoord);


            if (fenByte == 'q') instantiateFigure(QUEEN, BLACK, xCoord - 1, yCoord);
            if (fenByte == 'Q') instantiateFigure(QUEEN, WHITE, xCoord - 1, yCoord);
        }
    }

    @Override
    public void componentMoved(ComponentEvent e) {

    }

    @Override
    public void componentShown(ComponentEvent e) {

    }

    @Override
    public void componentHidden(ComponentEvent e) {

    }

    @Override
    public void mouseClicked(MouseEvent e)
    {
    }

    private boolean moveFigure(Move move)
    {
        if ((move.fromX == move.toX) && (move.fromY == move.toY))
            return false;

        instantiateFigure(boardMap[move.fromX][move.fromY][0], boardMap[move.fromX][move.fromY][1], move.toX, move.toY);
        instantiateFigure(NONE, NONE, move.fromX, move.fromY);


        return true;
    }

    private void highlightCell(int state, int x, int y)
    {
            highlightMap[x][y] = state;
    }

    private void clearHighlights()
    {
        for (int i = 0; i < 8; i++)
        {
            for (int j = 0; j < 8; j++)
            {
                highlightMap[i][j] = NONE;
            }
        }
    }

    private int getFigureColor(int x, int y)
    {
        if ((x >= 8) || (x < 0)) return NONE;
        if ((y >= 8) || (y < 0)) return NONE;

        return boardMap[x][y][1];
    }

    private int getFigureType(int x, int y)
    {
        if ((x >= 8) || (x < 0)) return NONE;
        if ((y >= 8) || (y < 0)) return NONE;


        return boardMap[x][y][0];
    }

    private boolean isLegalMove(Move move)
    {
        if ((getFigureColor(move.toX, move.toY) == getFigureColor(move.fromX, move.fromY)))
        {
            return false;
        }

        if (getFigureType(move.fromX, move.fromY) == PAWN)
        {
            int yDisplacement = getFigureColor(move.fromX, move.fromY) == WHITE ? -1 : 1;

            ArrayList<Move> legalMoves = new ArrayList<>();

            if (getFigureType(move.toX, move.toY) == NONE)
                legalMoves.add(new Move(move.fromX, move.fromY, move.fromX, move.fromY + yDisplacement));

            if (getFigureColor(move.fromX + 1, move.fromY + yDisplacement) == getOppositeColor(getFigureColor(move.fromX, move.fromY)))
                legalMoves.add(new Move(move.fromX, move.fromY, move.fromX + 1, move.fromY + yDisplacement));

            if (getFigureColor(move.fromX - 1, move.fromY + yDisplacement) == getOppositeColor(getFigureColor(move.fromX, move.fromY)))
                legalMoves.add(new Move(move.fromX, move.fromY, move.fromX - 1, move.fromY + yDisplacement));


            if (!hasMove(legalMoves, move))
                return false;
        }



        return true;
    }

    private int getOppositeColor(int color)
    {
        if (color == WHITE)
            return BLACK;
        if (color == BLACK)
            return WHITE;
        return NONE;
    }

    private void printMove(Move move)
    {
        System.out.println(" X1 - " + move.toX + " Y1 - " + move.toY);
        System.out.println(" X0 - " + move.fromX + " Y0 - " + move.fromY);
    }
    private boolean hasMove(ArrayList<Move> moves, Move move)
    {
        for (Move m : moves)
        {
            if (compareMoves(m, move)) return true;
        }
        return false;
    }

    private boolean compareMoves(Move a, Move b)
    {
        if (a.toY != b.toY) return false;
        if (a.toX != b.toX) return false;
        if (a.fromY != b.fromY) return false;
        if (a.fromX != b.fromX) return false;

        return true;
    }

    private void highlightMoveSequence(ArrayList<Move> sequence)
    {
        for (Move move : sequence)
        {
            highlightCell(ILLEGAL_MOVE,move.toX, move.toY);
        }
    }

    @Override
    public void mousePressed(MouseEvent e) {

        int x = Math.round(e.getX() / (cellWidth));
        int y = Math.round(e.getY() / (cellHeight));

        figureSelected[0] = x;
        figureSelected[1] = y;

        IsMousePressed = true;

        clearHighlights();
    }


    @Override
    public void mouseReleased(MouseEvent e)
    {
        int x = Math.round(e.getX() / (cellWidth));
        int y = Math.round(e.getY() / (cellHeight));

        IsMousePressed = false;

        Move move = new Move(figureSelected[0], figureSelected[1], x, y);

        if (isLegalMove(move))
        {
            moveFigure(move);
        }
        else
        {
            highlightCell(ILLEGAL_MOVE, move.fromX, move.fromY);
            highlightCell(ILLEGAL_MOVE, move.toX, move.toY);
        }

    }

    @Override
    public void mouseEntered(MouseEvent e) {

    }

    @Override
    public void mouseExited(MouseEvent e) {

    }
}
